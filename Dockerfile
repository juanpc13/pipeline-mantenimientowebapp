# ARG registry=192.168.0.15:5000
ARG registry=192.168.99.100:5000
ARG image="/payara/micro"
ARG tag=":5-SNAPSHOT"
FROM ${registry}${image}${tag}

COPY hello-world.war /opt/payara/deployments